package org.tuxjitl.usermanagementportal.constants;

public class SecurityConstant {

    //Set to 5 days (ms) : dependant on business requirements
    public static final long EXPIRATION_TIME = 432_000_000L;

    //after receiving token, no need for further verification
    public static final String TOKEN_PREFIX = "Bearer ";

    public static final String JWT_TOKEN_HEADER = "Jwt-Token";

    public static final String TOKEN_CAN_NOT_BE_VERIFIED = "Token cannot be verified";

    public static final String GET_ARRAYS_LLC = "Get Arrays, LLC";

    public static final String GET_ARRAYS_ADMINISTRATION = "User Management Portal";

    public static final String AUTHORITIES = "authorities";

    public static final String FORBIDDEN_MESSAGE = "You need to log in to access this page";

    public static final String ACCESS_DENIED_MESSAGE = "You do not have permission to access this resource";

    public static final String OPTIONS_HTTP_METHOD = "OPTIONS";//should be denied for user

    public static final String[] PUBLIC_URLS = {
            "/user/login",
            "/user/register",
            "/user/image/**"
    };
}
