package org.tuxjitl.usermanagementportal.constants;

public class UserImplementationConstant {

    public static final String USER_NAME_ALREADY_EXISTS = "User name already exists";
    public static final String EMAIL_ALREADY_EXISTS = "Email already exists";
    public static final String NO_USER_FOUND_BY_USER_NAME = "No user found by user name: ";
    public static final String RETURNING_FOUND_USER_BY_USERNAME = "Returning found user by username: ";
    public static final String NO_USER_FOUND_BY_EMAIL = "No user found for email: ";
    public static final String USER_DELETED_SUCCESSFULLY = "User deleted successfully";

    //TODO remove this when testing is done, just to see the password that should be send to email
    public static final String NEW_USER_PASSWORD = "New user password: ";


}
