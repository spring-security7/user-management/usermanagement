package org.tuxjitl.usermanagementportal.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.http.HttpStatus;

import java.util.Date;

/*
    This class is going to represent all the communication we send back

    No entity in the database
 */
public class HttpResponse {

//    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd hh:mm:ss",timezone="Europe/Brussels")
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd hh:mm:ss aa",
            timezone = "Europe/Brussels")
    private Date timeStamp;

    private int httpStatusCode;//200,201,300,400,500,...
    private HttpStatus httpStatus;
    private String reason;
    private String message;

    //to be removed=> default int = 0, string = null ==> not valid http stuff
    public HttpResponse() {

    }

    public HttpResponse(int httpStatusCode, HttpStatus httpStatus, String reason, String message) {

        //do not give the user the ability to create a timestamp
        this.timeStamp=new Date();

        this.httpStatusCode = httpStatusCode;
        this.httpStatus = httpStatus;
        this.reason = reason;
        this.message = message;
    }


    public Date getTimeStamp() {

        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {

        this.timeStamp = timeStamp;
    }


    public int getHttpStatusCode() {

        return httpStatusCode;
    }

    public void setHttpStatusCode(int httpStatusCode) {

        this.httpStatusCode = httpStatusCode;
    }

    public HttpStatus getHttpStatus() {

        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {

        this.httpStatus = httpStatus;
    }

    public String getReason() {

        return reason;
    }

    public void setReason(String reason) {

        this.reason = reason;
    }

    public String getMessage() {

        return message;
    }

    public void setMessage(String message) {

        this.message = message;
    }
}
