package org.tuxjitl.usermanagementportal.domain;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;


/*
    This class is used by Spring security

    We are going to map the User to this user so Spring security can work with it
 */
public class UserPrincipal implements UserDetails {

    private final User user;

    public UserPrincipal(User user) {

        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        return stream(this.user.getAuthorities())
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }

    @Override
    public String getPassword() {

        return this.user.getPassword();
    }

    @Override
    public String getUsername() {

        return this.user.getUserName();
    }

    @Override
    public boolean isAccountNonExpired() {

        //if false ==> login will always fail
        return true;//not used for now
    }

    @Override
    public boolean isAccountNonLocked() {

        return this.user.isNotLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {

        return true;//could be set to expire ==> business requirements
    }

    @Override
    public boolean isEnabled() {

        return this.user.isActive();
    }
}
