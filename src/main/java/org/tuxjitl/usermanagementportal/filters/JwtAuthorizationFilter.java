package org.tuxjitl.usermanagementportal.filters;


import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.tuxjitl.usermanagementportal.utilities.JWTTokenProvider;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.tuxjitl.usermanagementportal.constants.SecurityConstant.OPTIONS_HTTP_METHOD;
import static org.tuxjitl.usermanagementportal.constants.SecurityConstant.TOKEN_PREFIX;

/*
        Purpose:

        Authorize/deny any request from users

        Executes with every request and it only does this ones, not multiple times
 */

@Component
public class JwtAuthorizationFilter extends OncePerRequestFilter {

    private final JWTTokenProvider jwtTokenProvider;

    public JwtAuthorizationFilter(JWTTokenProvider jwtTokenProvider) {

        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {


        /*
                Option method is send before a request and getters info about the server

                Which info does the server accepts (security issue => info hackers)
         */
        if (request.getMethod().equalsIgnoreCase(OPTIONS_HTTP_METHOD)) {

            //if method = options, let it go through
            response.setStatus(HttpStatus.OK.value());
        }
        else {

            //if method is not options

            String authorizationHeader = request.getHeader(AUTHORIZATION);

            if (authorizationHeader == null || !authorizationHeader.startsWith(TOKEN_PREFIX)) {

                //do nothing with it (null or bearer) because this is not our authorization header
                filterChain.doFilter(request, response);
                return;
            }

            //it IS OUR authorization header

            //remove the prefix from header ==> token
            //string starting on index prefix.length
            String token = authorizationHeader.substring(TOKEN_PREFIX.length());
            String userName = jwtTokenProvider.getSubject(token);//can fail TODO

            //if using session ==> use also SecurityContextHolder
            if(jwtTokenProvider.isTokenValid(userName,token) &&
                    SecurityContextHolder.getContext().getAuthentication() == null){
                //user doesn't have an authentication

                //which authorities does the user have
                List<GrantedAuthority> authorities = jwtTokenProvider.getAuthorities(token);

                // get the authentication
                Authentication authentication =
                        jwtTokenProvider.getAuthentication(userName,authorities,request);

                //set authentication for the user
                SecurityContextHolder.getContext().setAuthentication(authentication);

                //Spring security now knows the user is an authenticated user

            }else{
                //token not valid or is not in security context

                //VERY IMPORTANT
                //clear context ==> make sure that nothing remains in the context
                SecurityContextHolder.clearContext();
            }

        }

        filterChain.doFilter(request,response);


    }
}
