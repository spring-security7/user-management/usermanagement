package org.tuxjitl.usermanagementportal.services;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;

import static java.util.concurrent.TimeUnit.MINUTES;

/*
    Purpose: to keep track of number of login attempts

    Brute force attacks - in memory cache (guava dependency)
 */

@Service
public class LoginAttemptService {

    private static final int MAXIMUM_NUMBER_OF_ATTEMPTS = 5;
    private static final int ATTEMPT_INCREMENT = 1;
    private static final int MAXIMUM_NUMBER_OF_ENTRIES_IN_CACHE = 100;
    private static final int MAXIMUM_DURATION_OF_CACHE = 15;

    private LoadingCache<String, Integer> loginAttemptCache;

    //initialize guava cache
    public LoginAttemptService() {

        super();
        //maximum size: 100 ENTRIES, cache expires after 15 minutes
        this.loginAttemptCache = CacheBuilder
                .newBuilder()
                .expireAfterWrite(MAXIMUM_DURATION_OF_CACHE, MINUTES)
                .maximumSize(MAXIMUM_NUMBER_OF_ENTRIES_IN_CACHE)
                .build(new CacheLoader<String, Integer>() {
                    public Integer load(String key) {

                        return 0;
                    }
                });
    }

    public void removeUserFromLoginAttemptCache(String userName) {

        loginAttemptCache.invalidate(userName);

    }

    public void addUserToLoginAttemptCache(String userName) {

        int attempts = 0;

        try {
            attempts = ATTEMPT_INCREMENT + loginAttemptCache.get(userName);
            loginAttemptCache.put(userName, attempts);
        }
        catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

    public boolean hasExceededMaxAttempts(String userName)  {


        try {
            return loginAttemptCache.get(userName) >= MAXIMUM_NUMBER_OF_ATTEMPTS;
        }
        catch (ExecutionException e) {
            e.printStackTrace();
        }

        return false;
    }
}
