package org.tuxjitl.usermanagementportal.services;


import org.springframework.web.multipart.MultipartFile;
import org.tuxjitl.usermanagementportal.domain.User;
import org.tuxjitl.usermanagementportal.exceptions.domain.*;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;

public interface UserService {

    User register(String firstName,String lastName,String userName,String email)
            throws UserNotFoundException, UserNameExistsException, EmailExistsException, MessagingException;

    List<User> getUsers();

    User findUserByUserName(String userName);

    User findUserByEmail(String email);

    //for admin when in the system
    User addNewUser(String firstName, String lastName, String username, String email, String role,
                    boolean isNonLocked, boolean isActive, MultipartFile profileImage)
            throws UserNotFoundException, UserNameExistsException, EmailExistsException, IOException, NotAnImageFileException;

    User updateUser(String currentUsername, String newFirstName, String newLastName, String newUsername,
                    String newEmail, String role, boolean isNonLocked, boolean isActive, MultipartFile profileImage)
            throws UserNotFoundException, UserNameExistsException, EmailExistsException, IOException,
            NotAnImageFileException;

    void deleteUser(String userName) throws IOException;


    void resetPassword(String email) throws MessagingException, EmailNotFoundException;

    User updateProfileImage(String username, MultipartFile profileImage)
            throws UserNotFoundException, UserNameExistsException, EmailExistsException, IOException,
            NotAnImageFileException;


}
