package org.tuxjitl.usermanagementportal.services.impl;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.tuxjitl.usermanagementportal.domain.User;
import org.tuxjitl.usermanagementportal.domain.UserPrincipal;
import org.tuxjitl.usermanagementportal.enumerations.Role;
import org.tuxjitl.usermanagementportal.exceptions.domain.*;
import org.tuxjitl.usermanagementportal.repositories.UserRepository;
import org.tuxjitl.usermanagementportal.services.EmailService;
import org.tuxjitl.usermanagementportal.services.LoginAttemptService;
import org.tuxjitl.usermanagementportal.services.UserService;

import javax.mail.MessagingException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.util.Arrays.asList;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.springframework.http.MediaType.*;
import static org.tuxjitl.usermanagementportal.constants.FileConstant.*;
import static org.tuxjitl.usermanagementportal.constants.UserImplementationConstant.*;
import static org.tuxjitl.usermanagementportal.enumerations.Role.ROLE_USER;

@Service
@Transactional
@Qualifier("userDetailsService")
public class UserServiceImpl implements UserService, UserDetailsService {


    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final LoginAttemptService loginAttemptService;
    private final EmailService emailService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder passwordEncoder,
                           LoginAttemptService loginAttemptService, EmailService emailService) {

        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.loginAttemptService = loginAttemptService;
        this.emailService = emailService;
    }


    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        User user = userRepository.findUserByUserName(userName);

        if (user == null) {

            logger.error(NO_USER_FOUND_BY_USER_NAME + userName);
            throw new UsernameNotFoundException(NO_USER_FOUND_BY_USER_NAME + userName);
        }
        else {

            validateLoginAttempt(user);

            user.setLastLoginDateDisplay(user.getLastLoginDate());

            //update last login date to now
            user.setLastLoginDate(new Date());

            userRepository.save(user);

            UserPrincipal userPrincipal = new UserPrincipal(user);
            logger.info(RETURNING_FOUND_USER_BY_USERNAME + userName);

            return userPrincipal;
        }

    }

    @Override
    public User register(String firstName, String lastName, String userName, String email)
            throws UserNotFoundException, UserNameExistsException, EmailExistsException, MessagingException {

        validateNewUserNameAndEmail(EMPTY, userName, email);

        String password = generatePassword();

        User user = createUser(firstName, lastName, userName, email, password, ROLE_USER.name(),
                true, true);

        userRepository.save(user);

        emailService.sendNewPasswordEmail(firstName, password, email);

        return user;
    }

    @Override
    public User addNewUser(String firstName, String lastName, String userName, String email, String role,
                           boolean isNotLocked, boolean isActive, MultipartFile profileImage)
            throws UserNotFoundException, UserNameExistsException, EmailExistsException, IOException,
            NotAnImageFileException {

        validateNewUserNameAndEmail(EMPTY, userName, email);
        String password = generatePassword();

        User user = createUser(firstName, lastName, userName, email, password, role, isNotLocked, isActive);

        userRepository.save(user);

        //don't save the image with the user statement unless everything passes
        saveProfileImage(user, profileImage);

        return user;
    }

    @Override
    public User updateUser(String currentUsername, String newFirstName, String newLastName,
                           String newUserName, String newEmail, String role, boolean isNotLocked,
                           boolean isActive, MultipartFile profileImage)
            throws UserNotFoundException, UserNameExistsException, EmailExistsException,
            IOException, NotAnImageFileException {

        //if validation fails => throws user not found exception
        User currentUser = validateNewUserNameAndEmail(currentUsername, newUserName, newEmail);

        if (currentUser == null) {
            throw new UserNotFoundException(NO_USER_FOUND_BY_USER_NAME + currentUsername);
        }

        currentUser.setFirstName(newFirstName);
        currentUser.setLastName(newLastName);
        currentUser.setUserName(newUserName);
        currentUser.setEmail(newEmail);
        currentUser.setActive(isActive);
        currentUser.setNotLocked(isNotLocked);
        currentUser.setRole(getRoleEnumName(role).name());
        currentUser.setAuthorities(getRoleEnumName(role).getAuthorities());

        userRepository.save(currentUser);

        //don't save the image with the user statement unless everything passes
        saveProfileImage(currentUser, profileImage);

        return currentUser;
    }


    @Override
    public List<User> getUsers() {

        return userRepository.findAll();
    }

    @Override
    public User findUserByUserName(String userName) {

        return userRepository.findUserByUserName(userName);
    }

    @Override
    public User findUserByEmail(String email) {

        return userRepository.findUserByEmail(email);
    }

    @Override
    public void deleteUser(String userName) throws IOException {
        User user = userRepository.findUserByUserName(userName);
        Path userFolder = Paths.get(USER_FOLDER + user.getUserName()).toAbsolutePath().normalize();
        FileUtils.deleteDirectory(new File(userFolder.toString()));
        userRepository.deleteById(user.getId());
    }


    @Override
    public void resetPassword(String email) throws MessagingException, EmailNotFoundException {

        //find the user
        User user = userRepository.findUserByEmail(email);

        if (user == null) {
            throw new EmailNotFoundException(NO_USER_FOUND_BY_EMAIL + email);
        }

        String newPassword = generatePassword();
        user.setPassword(encodePassword(newPassword));

        userRepository.save(user);
        emailService.sendNewPasswordEmail(user.getFirstName(), newPassword, user.getEmail());
    }

    @Override
    public User updateProfileImage(String userName, MultipartFile newProfileImage)
            throws UserNotFoundException, UserNameExistsException, EmailExistsException, IOException,
            NotAnImageFileException {

        //check an existing user that want's to update the profile picture
        User user = validateNewUserNameAndEmail(userName, null, null);

        if (user == null) {
            throw new UserNotFoundException(NO_USER_FOUND_BY_USER_NAME + userName);
        }

        saveProfileImage(user, newProfileImage);

        return user;
    }


    //****************************************************************************
    //                      HELPER METHODS
    //****************************************************************************

    private User createUser(String firstName, String lastName, String userName, String email, String password,
                            String role, boolean isNotLocked, boolean isActive) {

        User user = new User();

        user.setUserId(generateUserId());
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setJoinDate(new Date());
        user.setUserName(userName);
        user.setEmail(email);
        user.setPassword(encodePassword(password));
        user.setActive(isActive);
        user.setNotLocked(isNotLocked);
        user.setRole(getRoleEnumName(role).name());
        user.setAuthorities(getRoleEnumName(role).getAuthorities());
        user.setProfileImageUrl(getTemporaryProfileImageUrl(userName));
        return user;
    }

    private String encodePassword(String password) {

        return passwordEncoder.encode(password);
    }

    private String generatePassword() {

        return RandomStringUtils.randomAlphabetic(10);
    }

    private String generateUserId() {

        return RandomStringUtils.randomNumeric(10);
    }

    private void validateLoginAttempt(User user) {

        if (user.isNotLocked()) {
            user.setNotLocked(!loginAttemptService.hasExceededMaxAttempts(user.getUserName()));
        }
        else {
            loginAttemptService.removeUserFromLoginAttemptCache(user.getUserName());
        }

    }

    private User validateNewUserNameAndEmail(String currentUserName, String newUserName, String newEmail)
            throws UserNameExistsException, EmailExistsException, UserNotFoundException {

        User userByNewUserName = findUserByUserName(newUserName);
        User userByNewEmail = findUserByEmail(newEmail);

        //if user already exists? (perhaps the user does an update)
        if (StringUtils.isNotBlank(currentUserName)) {

            User currentUser = findUserByUserName(currentUserName);

            //if null, user does not exist in db
            if (currentUser == null) {
                throw new UserNotFoundException(NO_USER_FOUND_BY_USER_NAME + ": " + currentUserName);
            }

            //if the username already exists, but different users
            if (userByNewUserName != null && !currentUser.getId().equals(userByNewUserName.getId())) {
                throw new UserNameExistsException(USER_NAME_ALREADY_EXISTS);
            }

            //if email already exists, but different users
            if (userByNewEmail != null && !currentUser.getId().equals(userByNewEmail.getId())) {
                throw new EmailExistsException(EMAIL_ALREADY_EXISTS);
            }

            return currentUser;
        }
        else {

            //brand new user
            if (userByNewUserName != null) {
                throw new UserNameExistsException(USER_NAME_ALREADY_EXISTS);
            }

            if (userByNewEmail != null) {
                throw new EmailExistsException(EMAIL_ALREADY_EXISTS);
            }

            return null;
        }

    }

    private String getTemporaryProfileImageUrl(String userName) {

        return ServletUriComponentsBuilder.fromCurrentContextPath().
                path(DEFAULT_USER_IMAGE_PATH + userName).toUriString();

    }

    private Role getRoleEnumName(String role) {

        return Role.valueOf(role.toUpperCase());
    }

    private void saveProfileImage(User user, MultipartFile profileImage) throws IOException, NotAnImageFileException {

        if (profileImage != null) {

            //check image extension for upload
            if(!asList(IMAGE_JPEG_VALUE,IMAGE_PNG_VALUE,IMAGE_GIF_VALUE).contains(profileImage.getContentType())){
                throw new NotAnImageFileException(profileImage.getOriginalFilename() +
                        NOT_AN_IMAGE_FILE);
            }

            //find location user folder
            Path userFolder = Paths
                    .get(USER_FOLDER + user.getUserName())
                    .toAbsolutePath()
                    .normalize();

            //if home folder does not exist ==> create it
            if (!Files.exists(userFolder)) {
                Files.createDirectories(userFolder);
                logger.info(DIRECTORY_CREATED + userFolder);
            }

            //if home folder exists ==> delete content
            Files.deleteIfExists(Paths.get(userFolder + user.getUserName() + DOT + JPG_EXTENSION));

            Files.copy(profileImage.getInputStream(),
                    userFolder.resolve(user.getUserName() + DOT + JPG_EXTENSION),
                            REPLACE_EXISTING);

            user.setProfileImageUrl(setNewProfileImageUrl(user.getUserName()));
            userRepository.save(user);
            logger.info(FILE_SAVED_IN_FILE_SYSTEM + profileImage.getOriginalFilename());
        }

    }

    private String setNewProfileImageUrl(String userName) {
        return ServletUriComponentsBuilder.fromCurrentContextPath().path(USER_IMAGE_PATH + userName + FORWARD_SLASH
                + userName + DOT + JPG_EXTENSION).toUriString();
    }
}
