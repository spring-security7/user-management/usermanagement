package org.tuxjitl.usermanagementportal.utilities;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.tuxjitl.usermanagementportal.domain.UserPrincipal;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import static java.util.Arrays.stream;
import static org.tuxjitl.usermanagementportal.constants.SecurityConstant.*;


@Component
public class JWTTokenProvider {

    //normally stored in a secure server and retrieved in e.g. property file
    @Value("${jwt.secret}")
    private String secret;

    //create token for specific user to send to user
    public String generateJwtToken(UserPrincipal userPrincipal) {
        //get the claims
        String[] claims = getClaimsForUser(userPrincipal);


        return JWT.create().withIssuer(GET_ARRAYS_LLC)
                .withAudience(GET_ARRAYS_ADMINISTRATION)
                .withIssuedAt(new Date())
                .withSubject(userPrincipal.getUsername())
                .withArrayClaim(AUTHORITIES, claims)
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(HMAC512(secret.getBytes()));
    }

    //get the autorities from the token received from user
    public List<GrantedAuthority> getAuthorities(String token) {

        String[] claims = getClaimsFromToken(token);

        return stream(claims)
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }

    //to set authentication when authenticated in security context
    //once this is done successfully => spring security context knows user is authenticated
    public Authentication getAuthentication(String username, List<GrantedAuthority> authorities,
                                            HttpServletRequest request) {

        UsernamePasswordAuthenticationToken usernamePasswordAuthToken =
                new UsernamePasswordAuthenticationToken(username, null, authorities);

        //set the details on the token
        //setup information about the user in the spring security context
        usernamePasswordAuthToken.setDetails(new WebAuthenticationDetailsSource()
                .buildDetails(request));

        return usernamePasswordAuthToken;
    }

    //check if token is valid
    public boolean isTokenValid(String username, String token) {

        JWTVerifier jwtVerifier = getJWTVerifier();

        return StringUtils.isNotEmpty(username) && !isTokenExpired(jwtVerifier, token);
    }

    //get payload
    public String getSubject(String token) {

        JWTVerifier jwtVerifier = getJWTVerifier();

        return jwtVerifier.verify(token).getSubject();
    }


    //*******************************************************************************************
    //                  HELPER METHODS
    //*******************************************************************************************


    private boolean isTokenExpired(JWTVerifier jwtVerifier, String token) {

        Date expirationDate = jwtVerifier.verify(token).getExpiresAt();

        return expirationDate.before(new Date());

    }

    private String[] getClaimsFromToken(String token) {

        JWTVerifier jwtVerifier = getJWTVerifier();
        return jwtVerifier.verify(token).getClaim(AUTHORITIES).asArray(String.class);
    }

    private JWTVerifier getJWTVerifier() {

        JWTVerifier jwtVerifier;

        try {
            Algorithm algorithm = HMAC512(secret);
            jwtVerifier = JWT.require(algorithm).withIssuer(GET_ARRAYS_LLC).build();
        }
        catch (JWTVerificationException e) {
            //throw this to hide inner workings from user
            throw new JWTVerificationException(TOKEN_CAN_NOT_BE_VERIFIED);
        }

        return jwtVerifier;
    }


    private String[] getClaimsForUser(UserPrincipal userPrincipal) {

        List<String> authorities = new ArrayList<>();

        for (GrantedAuthority grantedAuthority : userPrincipal.getAuthorities()) {
            authorities.add(grantedAuthority.getAuthority());
        }

        // return it as an array instead of a list
        return authorities.toArray(new String[0]);
    }

}
