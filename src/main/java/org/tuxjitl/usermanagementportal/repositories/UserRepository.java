package org.tuxjitl.usermanagementportal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.tuxjitl.usermanagementportal.domain.User;


public interface UserRepository extends JpaRepository<User,Long> {

    User findUserByUserName(String userName);
    User findUserByEmail(String email);


}
