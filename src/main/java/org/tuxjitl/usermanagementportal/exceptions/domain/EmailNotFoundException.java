package org.tuxjitl.usermanagementportal.exceptions.domain;

public class EmailNotFoundException extends Exception{

    public EmailNotFoundException(String message) {

        super(message);
    }
}
