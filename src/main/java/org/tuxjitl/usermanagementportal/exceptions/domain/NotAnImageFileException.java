package org.tuxjitl.usermanagementportal.exceptions.domain;

public class NotAnImageFileException extends Exception {

    public NotAnImageFileException(String message) {
        super(message);
    }

}
