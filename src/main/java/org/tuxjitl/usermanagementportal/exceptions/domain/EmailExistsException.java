package org.tuxjitl.usermanagementportal.exceptions.domain;

public class EmailExistsException extends Exception{

    public EmailExistsException(String message) {

        super(message);
    }
}
