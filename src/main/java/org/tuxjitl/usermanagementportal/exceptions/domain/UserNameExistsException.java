package org.tuxjitl.usermanagementportal.exceptions.domain;

public class UserNameExistsException extends Exception{

    public UserNameExistsException(String message) {

        super(message);
    }
}
