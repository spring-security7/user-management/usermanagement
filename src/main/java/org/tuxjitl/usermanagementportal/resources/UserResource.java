package org.tuxjitl.usermanagementportal.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.tuxjitl.usermanagementportal.domain.HttpResponse;
import org.tuxjitl.usermanagementportal.domain.User;
import org.tuxjitl.usermanagementportal.domain.UserPrincipal;
import org.tuxjitl.usermanagementportal.exceptions.ExceptionHandling;
import org.tuxjitl.usermanagementportal.exceptions.domain.*;
import org.tuxjitl.usermanagementportal.services.UserService;
import org.tuxjitl.usermanagementportal.utilities.JWTTokenProvider;

import javax.mail.MessagingException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.IMAGE_JPEG_VALUE;
import static org.tuxjitl.usermanagementportal.constants.EmailConstant.EMAIL_SEND_MESSAGE;
import static org.tuxjitl.usermanagementportal.constants.FileConstant.*;
import static org.tuxjitl.usermanagementportal.constants.SecurityConstant.JWT_TOKEN_HEADER;
import static org.tuxjitl.usermanagementportal.constants.UserImplementationConstant.USER_DELETED_SUCCESSFULLY;

@RestController
@RequestMapping(value = {"/", "/user"})
public class UserResource extends ExceptionHandling {



    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final JWTTokenProvider jwtTokenProvider;

    @Autowired
    public UserResource(UserService userService, AuthenticationManager authenticationManager,
                        JWTTokenProvider jwtTokenProvider) {

        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @PostMapping("/register")
    public ResponseEntity<User> registerUser(@RequestBody User aUser) throws UserNotFoundException,
            UserNameExistsException, EmailExistsException, MessagingException {

        User newUser = userService.register(aUser.getFirstName(), aUser.getLastName(),
                aUser.getUserName(), aUser.getEmail());


        return new ResponseEntity<>(newUser, OK);

    }

    @PostMapping("/login")
    public ResponseEntity<User> login(@RequestBody User aUser) {

        /*
            If there is any problem (wrong password, account locked, ...
            this method will throw an exception and the rest of the code
            will not be executed.
         */
        authenticate(aUser.getUserName(), aUser.getPassword());

        // When authentication ok, find user by username
        User logginUser = userService.findUserByUserName(aUser.getUserName());

        // If we have a user, create principal
        UserPrincipal userPrincipal = new UserPrincipal(logginUser);
        // Generate JWT and put into header
        HttpHeaders jwtHeader = getJwtHeader(userPrincipal);

        // Return user with Jwt to user
        return new ResponseEntity<>(logginUser, jwtHeader, OK);

    }

    //When you are a internal user and have the permissions to do so, create an account
    @PostMapping("/add")
    public ResponseEntity<User> addNewUser(@RequestParam("firstName") String firstName,
               @RequestParam("lastName") String lastName,
               @RequestParam("userName") String userName,
               @RequestParam("email") String email,
               @RequestParam("role") String role,
               @RequestParam("isActive") String isActive,
               @RequestParam("isNotLocked") String isNotLocked,
               @RequestParam(value = "profileImage",required = false) MultipartFile profileImage
    ) throws UserNotFoundException, UserNameExistsException, EmailExistsException, IOException, NotAnImageFileException {

        User newUser = userService.addNewUser(firstName,
                lastName,userName,email,role,Boolean.parseBoolean(isNotLocked),
                Boolean.parseBoolean(isActive),profileImage);

        return new ResponseEntity<>(newUser,OK);

    }

    //When you are a internal user and have the permissions to do so, update an account
    @PostMapping("/update")
    public ResponseEntity<User> updateUser(@RequestParam("currentUserName") String currentUserName,
                                           @RequestParam("firstName") String firstName,
                                           @RequestParam("lastName") String lastName,
                                           @RequestParam("userName") String userName,
                                           @RequestParam("email") String email,
                                           @RequestParam("role") String role,
                                           @RequestParam("isActive") String isActive,
                                           @RequestParam("isNotLocked") String isNotLocked,
                                           @RequestParam(value = "profileImage",required = false) MultipartFile profileImage
    ) throws UserNotFoundException, UserNameExistsException, EmailExistsException, IOException, NotAnImageFileException {

        User updatedUser = userService.updateUser(currentUserName,firstName,
                lastName,userName,email,role,Boolean.parseBoolean(isNotLocked),
                Boolean.parseBoolean(isActive),profileImage);

        //ResponseEntity represents the request, the body, the headers
        return new ResponseEntity<>(updatedUser,OK);

    }

    @GetMapping("/find/{userName}")
    public ResponseEntity<User> getUserByUsrname(@PathVariable("userName") String userName){

        User foundUser = userService.findUserByUserName(userName);

        return new ResponseEntity<>(foundUser,OK);
    }

    @GetMapping("/list")
    public ResponseEntity<List<User>> getAllUsers(){

        List<User> users = userService.getUsers();

        return new ResponseEntity<>(users,OK);
    }

    @GetMapping("/resetPassword/{email}")
    public ResponseEntity<HttpResponse> resetPasswordByEmail(@PathVariable("email") String email)
            throws EmailNotFoundException, MessagingException {

        userService.resetPassword(email);

        return response(OK, EMAIL_SEND_MESSAGE + email);
    }


    @DeleteMapping("/delete/{userName}")
    @PreAuthorize("hasAnyAuthority('user:delete')")
    public ResponseEntity<HttpResponse> deleteUser(@PathVariable("userName") String userName) throws IOException {
        userService.deleteUser(userName);
        return response(OK, USER_DELETED_SUCCESSFULLY);
    }



    @PostMapping("/updateProfileImage")
    public ResponseEntity<User> updateUserProfileImage(@RequestParam("userName") String userName,
                   @RequestParam(value = "profileImage") MultipartFile profileImage
    ) throws UserNotFoundException, UserNameExistsException, EmailExistsException, IOException, NotAnImageFileException {

        User user = userService.updateProfileImage(userName,profileImage);

        //ResponseEntity represents the request, the body, the headers
        return new ResponseEntity<>(user,OK);

    }

    //when the user has provided an image ==> read from the system
    @GetMapping(path = "/image/{userName}/{fileName}",produces = IMAGE_JPEG_VALUE)
    public byte[] getProfileImage(@PathVariable("userName") String userName,
                                  @PathVariable("fileName") String filename) throws IOException {

        return Files.readAllBytes(Paths.get(USER_FOLDER + userName + FORWARD_SLASH + filename));
    }

    //when robohash has provided the image ==> a temp image ==> read it from the internet
    @GetMapping(path = "/image/profile/{userName}",produces = IMAGE_JPEG_VALUE)
    public byte[] getTempProfileImage(@PathVariable("userName") String userName) throws IOException {

        URL url = new URL(TEMP_PROFILE_IMAGE_BASE_URL + userName);

        //stream image into byte array for output streaming //> store the actual data
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        //try with resources
        try(InputStream inputStream = url.openStream()){

            int bytesRead;

            //when stream is open, read x nmbr of bytes at a time, not all bytes at ones
            //a buffer
            byte[] chunck = new byte[1024];

            while((bytesRead = inputStream.read(chunck)) > 0){

                byteArrayOutputStream.write(chunck,0,bytesRead);
            }


        }

        return byteArrayOutputStream.toByteArray();
    }






    //***************************************************************************************************
    //                              HELPER METHODS
    //***************************************************************************************************


    private ResponseEntity<HttpResponse> response(HttpStatus httpStatus, String message) {

        return new ResponseEntity<>(new HttpResponse(httpStatus.value(),
                httpStatus,httpStatus.getReasonPhrase().toUpperCase(), message.toUpperCase()),httpStatus);

    }

    private HttpHeaders getJwtHeader(UserPrincipal userPrincipal) {

        HttpHeaders headers = new HttpHeaders();
        headers.add(JWT_TOKEN_HEADER, jwtTokenProvider.generateJwtToken(userPrincipal));

        return headers;

    }

    private void authenticate(String userName, String password) {

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userName,password));

    }


}
