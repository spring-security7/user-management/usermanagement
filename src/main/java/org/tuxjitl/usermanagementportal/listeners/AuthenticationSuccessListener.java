package org.tuxjitl.usermanagementportal.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;
import org.tuxjitl.usermanagementportal.domain.UserPrincipal;
import org.tuxjitl.usermanagementportal.services.LoginAttemptService;

@Component
public class AuthenticationSuccessListener {

    private final LoginAttemptService loginAttemptService;

    @Autowired
    public AuthenticationSuccessListener(LoginAttemptService loginAttemptService) {

        this.loginAttemptService = loginAttemptService;
    }

    @EventListener
    public void onAuthenticationSuccess(AuthenticationSuccessEvent event){

        /*
            principal here is an user

            comes from UserResource => authenticate methode ==> username

            but chained methods (getPrincipal) of event gives back an user
         */
        Object principal = event.getAuthentication().getPrincipal();

        //a safety check, in case event returns something else than an user
        if(principal instanceof UserPrincipal){
            UserPrincipal userPrincipal = (UserPrincipal) event.getAuthentication().getPrincipal();
            loginAttemptService.removeUserFromLoginAttemptCache(userPrincipal.getUsername());
        }
    }
}
