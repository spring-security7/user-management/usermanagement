package org.tuxjitl.usermanagementportal.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;
import org.tuxjitl.usermanagementportal.services.LoginAttemptService;

@Component
public class AuthenticationFailureListener {

    private final LoginAttemptService loginAttemptService;

    @Autowired
    public AuthenticationFailureListener(LoginAttemptService loginAttemptService) {

        this.loginAttemptService = loginAttemptService;
    }

    @EventListener
    public void onAuthenticationFailure(AuthenticationFailureBadCredentialsEvent event){

        /*
            principal here is a string

            comes from UserResource => authenticate methode ==> username

            but chained methods of event gives back an object
         */
        Object principal = event.getAuthentication().getPrincipal();

        //a safety check, in case event returns something else than a string
        if(principal instanceof String){
            String username = (String) event.getAuthentication().getPrincipal();
            loginAttemptService.addUserToLoginAttemptCache(username);
        }
    }
}
