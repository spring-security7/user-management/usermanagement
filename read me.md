This demo app was developed against a docker MySql server (8). 
It also has mailing capabilities (see constants package).

The frontend application is an Angular 12 application. Here is an overview of the technologies involved:

Java 11
Spring
Spring oAuth 2 JWT
HTML
CSS
JS
Bootstrap 4.6
Angular 12
TypeScript
MySql (docker)
SubSink
JSON
File
slf4j
Apache Commons Lang
etc.